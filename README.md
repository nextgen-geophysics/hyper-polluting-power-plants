# Global map of CO2 emissions from power plants

This repository contains a interactive webmap that displays global power plants with associated CO2 emissions.

**[VIEW MAP](https://hyper-polluting.nextgengeo.com/)**

## References
- [Data source](https://iopscience.iop.org/article/10.1088/1748-9326/ac13f1)
